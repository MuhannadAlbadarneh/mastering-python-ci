# Dummy tests that are slow because they sleep a lot

import time
import pytest


def test_slow_addition():
    time.sleep(5)
    assert 2 + 2 == 4


def test_slow_subtraction():
    time.sleep(3)
    assert 10 - 5 == 5


@pytest.mark.parametrize("sleep_time", [1, 2, 3, 4, 5])
def test_slow_multiplication(sleep_time):
    time.sleep(sleep_time)
    assert 3 * 5 == 15
