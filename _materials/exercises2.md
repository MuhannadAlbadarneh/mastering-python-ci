# Exercises part 2

## Setup

Check out the `exercises2` branch of the repository (`git checkout exercises2`)

If the above doesn't work, copy the content of `part2-start` to the root level of your project to make sure GitLab CI will pick up the `.gitlab-ci.yml` configuration and all the files necessary to run the pipeline. Make sure to **show the hidden files**, otherwise you might not see files starting with a dot (like the `.gitlab-ci.yml` one).

## Exercises

Let's try to implement some improvements we just discussed:

1. First, push the existing code to GitLab to see how long it takes to run the pipeline without any improvements.
2. Run slow tests in a separate job
    - Mark some of the slow tests with `@pytest.mark.slow`.
    - Register this marker in `pytest.ini` file, so pytest won't complain about an unknown mark:
        ```
        markers =
            slow: marks tests as slow (deselect with '-m "not slow"')
        ```
    - Add another job called "test-slow"
    - Make the existing "test" job run tests that are not marked as slow (`pytest -m "not slow"`) and the new "test-slow" run only tests marked as slow (`pytest -m "slow"`)
    - Push your code to GitLab. Is the pipeline running faster now?
3. Enable pushing docker images at the end of the build job and pulling it at the beginning of the test jobs
    - Once you add the command to push/pull docker images to `.gitlab-ci.yml` file, you need to add the same image in the `docker-compose.yml` file
