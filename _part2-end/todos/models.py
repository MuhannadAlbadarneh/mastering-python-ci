from django.db import models


class Task(models.Model):
    title = models.CharField(max_length=255)
    done = models.BooleanField(default=False)

    def __str__(self):
        return f"{self.title}({'✔' if self.done else '✘'})"

    def is_done(self):
        return self.done == True
