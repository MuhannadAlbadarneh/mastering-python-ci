from django.test import TestCase
from django.urls import reverse
from .models import Task


def create_task(title):
    return Task.objects.create(title=title)


class TodoModelTests(TestCase):
    def test_new_tasks_are_not_done(self):
        """Test that a new task is not done by default."""
        task = Task(title="buy milk")
        self.assertFalse(task.is_done())


class TodoIndexViewTests(TestCase):
    def test_list_of_tasks(self):
        task = create_task("buy milk")

        response = self.client.get(reverse("todos:index"))

        self.assertEqual(response.status_code, 200)
        self.assertQuerysetEqual(response.context["all_tasks"], [task])
