"""Dice roller simulator with support for premium users.

Usage:
>>> python dice.py

Enable PRO mode to use 9-sided dice (only available in the programming world!) instead of the free, 6-sided ones:
>>> python dice.py --pro
"""

import argparse
from assets import welcome, dice
from random import randint


def main():
    # Parse the input options to determine if we're in a PRO mode
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--pro", action="store_true", help="Are we in the PRO mode?")
    args = parser.parse_args()

    # Ask for the total number of rolls and roll the dice
    number_of_rolls = start_game()
    results = multiple_rolls(number_of_rolls, args.pro)

    # Print results
    print_results(results)


def single_roll(pro=False):
    """Roll a single die.

    :param pro: enables 9-sided dice instead of the standard 6-sided ones.
    """
    max_pips = 9 if pro else 6
    return randint(1, max_pips)


def multiple_rolls(number_of_rolls, pro=False):
    """Keep rolling rolling rolling rolling until we get all the results."""
    return [single_roll(pro) for _ in range(int(number_of_rolls))]


def start_game():
    """Print welcome message and ask user how many dice we roll."""
    print("Welcome to the DICE ROLLER 3000")
    print(welcome)
    number_of_rolls = input("How many dice you want to throw?")

    # number_of_rolls is a string, so we need to converst it to int
    return int(number_of_rolls)


def print_results(results):
    """Display the ASCII art of the corresponding dice."""
    for result in results:
        print(dice[result])
    # TODO: Homework for you, dear reader:
    # How can we display dice horizontally instead of vertically?


if __name__ == "__main__":
    main()
